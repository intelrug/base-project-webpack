const ghPages = require('gh-pages');

ghPages.publish('dist', {
	src: [
		'**/*',
		'!robots.txt'
	],
	branch: 'dist'
}, e => throw e);
