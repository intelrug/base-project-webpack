const fs = require('fs');
const path = require('path');
const globby = require('globby');

const MAIN_FILE = path.resolve(__dirname, '../../app/resources/assets/scripts/common-template.js');
const REPLACE_PATTERN = '/* auto-injected code */';
const INDENT_PATTERN = /\n(.*)\/\* auto-injected code \*\//;

const concatCommonJsTransformer = async function (code) {
	let content = fs.readFileSync(MAIN_FILE).toString();
	const match = content.match(INDENT_PATTERN);
	let indent = '';
	if (match && match[1]) {
		indent = match[1];
	}
	content = content.replace(REPLACE_PATTERN, code.replace(/\n/g, '\n' + indent));

	const filePaths = await globby(['app/blocks/**/global-*.js']);
	const globCode = filePaths.reduce((acc, filePath) => {
		return acc + fs.readFileSync(filePath).toString() + '\n';
	}, '');
	return globCode + content;
};

module.exports = concatCommonJsTransformer;

