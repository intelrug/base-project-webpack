const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

const correctNumber = number => number < 10 ? '0' + number : number;

const getDateTime = () => {
	const now = new Date();
	const year = now.getFullYear();
	const month = correctNumber(now.getMonth() + 1);
	const day = correctNumber(now.getDate());
	const hours = correctNumber(now.getHours());
	const minutes = correctNumber(now.getMinutes());

	return `${year}-${month}-${day}-${hours}${minutes}`;
};

const datetime = process.env.ZIP_DATE_TIME ? '-' + getDateTime() : '';
const zipName = 'dist' + datetime + '.zip';

const output = fs.createWriteStream(path.join(__dirname, '../dist/' + zipName));
const archive = archiver('zip', {
	zlib: {level: 9} // Sets the compression level.
});

// listen for all archive data to be written
// 'close' event is fired only when a file descriptor is involved
output.on('close', function () {
	console.log('Archive was created successfully!');
	console.log((archive.pointer() / (1024 * 1024)).toFixed(2) + 'MB in total.');
});

archive.on('warning', function (err) {
	console.warn(err);
});

archive.on('error', function (err) {
	throw err;
});

archive.pipe(output);
archive.glob('**/*', {
	cwd: path.join(__dirname, '../dist'),
	ignore: ['dist.zip']
});
archive.finalize();
