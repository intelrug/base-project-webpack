/* eslint-disable no-process-exit,complexity */
require('dotenv').config();
const path = require('path');
const ftp = require('vinyl-ftp');
const fs = require('vinyl-fs');
const SFTP = require('ssh2-sftp-client');

(async () => {
	const USE_SSH = process.env.FTP_USE_SSH === 'true';
	const CLEAR_DEST = process.env.FTP_CLEAR_DEST === 'true';

	const HOST = process.env.FTP_HOST || '127.0.0.1';
	const USER = process.env.FTP_USER || 'root';
	const PASS = process.env.FTP_PASS || '';
	const PORT = process.env.FTP_PORT || (process.env.FTP_USE_SSH && 22) || 21;

	const SRC = process.env.FTP_SRC || './dist';
	const FULL_SRC = path.resolve(__dirname, '../', SRC);
	const DEST = process.env.FTP_DEST || './public_html';

	if (USE_SSH) {
		const client = new SFTP();
		await client.connect({
			host: HOST,
			username: USER,
			password: PASS,
			port: PORT
		});

		try {
			const destExist = await client.exists(DEST);
			if (!destExist) {
				await client.mkdir(DEST, true);
			}

			if (CLEAR_DEST) {
				const paths = await client.list(DEST);
				for (let i = 0; i < paths.length; ++i) {
					const p = paths[i];
					try {
						if (p.type === 'd') {
							await client.rmdir(path.join(DEST, p.name).replace('\\', '/'), true);
						} else {
							await client.delete(path.join(DEST, p.name).replace('\\', '/'));
						}
					} catch (e) {
						console.warn(e);
					}
				}
			}

			await client.uploadDir(FULL_SRC, DEST);
		} catch (e) {
			console.error(e);
			process.exit(1);
		} finally {
			await client.end();
		}

	} else {
		const connection = ftp.create({
			host: HOST,
			user: USER,
			password: PASS,
			port: PORT
		});

		if (CLEAR_DEST) {
			connection.clean(path.join(DEST, '**'), SRC);
		}

		return fs.src(path.join(SRC, '**'), {base: 'dist', buffer: false})
			.pipe(connection.newer(DEST))
			.pipe(connection.dest(DEST));
	}
})();
