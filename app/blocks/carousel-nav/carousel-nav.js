(function carouselNav() {
	var $carouselNavTrigger = $('.js-carousel-prev, .js-carousel-next'),
		$carouselNavs = $('.carousel-nav');

	$(document).ready(function () {
		$carouselNavs.each(function () {
			var scope = $(this).data('scope'),
				target = $(this).data('target'),
				$carousel = $();

			if (scope) {
				$carousel = $(this).closest(scope).find(target);
			}

			if ($carousel.length) {
				var slidesPerView = $carousel[0].swiper.params.slidesPerView,
					slides = $carousel.find('.swiper-slide').length;

				if (slides <= slidesPerView) {
					$(this).css({
						display: 'none'
					});
				} else {
					$(this).css({
						display: 'flex'
					});
				}
			}
		});
	});

	$carouselNavTrigger.on('click', function () {
		var $carouselNav = $(this).closest('.carousel-nav');

		if ($carouselNav.length) {
			var scope = $carouselNav.data('scope'),
				target = $carouselNav.data('target'),
				$carousel = $();

			if (scope) {
				$carousel = $(this).closest(scope).find(target);
			}

			if ($carousel.length) {
				if ($(this).hasClass('js-carousel-prev')) {
					$carousel[0].swiper.slidePrev();
				} else {
					$carousel[0].swiper.slideNext();
				}
			}
		}

		return false;
	});
})();
