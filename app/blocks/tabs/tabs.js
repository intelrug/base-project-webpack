(function tabs() {
	function changeTab($tabs, $triggers, $panels, activeIndex) {
		if ($tabs.length && $triggers.length && $panels.length) {
			$triggers.removeClass('tabs__label_state_active').eq(activeIndex).addClass('tabs__label_state_active');
			$panels.removeClass('tabs__panel_state_active').eq(activeIndex).addClass('tabs__panel_state_active');
		}
	}

	function getActiveIndex($tabs, $triggers, $currentTrigger) {
		let activeIndex = 0;

		activeIndex = $triggers.index($currentTrigger);

		return activeIndex;
	}


	const $tabs = $('.tabs');

	if ($tabs.length) {
		$tabs.each(function () {
			const $currentTabs = $(this);
			const $triggers = $(this).find('.tabs__label');
			const $panels = $(this).find('.tabs__panel');
			const $currentTrigger = $triggers.find('.tabs__label_state_active');
			let activeIndex = 0;

			if ($currentTrigger.lenght) {
				activeIndex = getActiveIndex($tabs, $triggers, $currentTrigger);
			}

			$panels.addClass('tabs__panel_state_hidden');

			changeTab($currentTabs, $triggers, $panels, activeIndex);

			$triggers.on('click', function () {
				if (!($(this).hasClass('tabs__label_state_disabled'))) {
					activeIndex = getActiveIndex($tabs, $triggers, $(this));
					changeTab($currentTabs, $triggers, $panels, activeIndex);
				}

				return false;
			});
		});
	}
})();
