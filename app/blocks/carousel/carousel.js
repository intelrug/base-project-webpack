(function carousel() {
	// eslint-disable-next-line no-unused-vars
	var mySwiper = new Swiper('.js-default-carousel', {
		speed: 300,
		loop: true,
		slidesPerView: 3,
		slidesPerGroup: 1
	});
})();
