(function page() {
	var $body = $('.page__body'),
		$footer = $('.page__footer');

	function setFooterOffset() {
		var footerHeight = $footer.outerHeight();

		$body.css({
			marginBottom: footerHeight
		});
	}

	$footer.addClass('page__footer_type_sticky');

	if ($body.length && $footer.length) {
		setFooterOffset();

		$(window).resize(function () {
			setFooterOffset();
		});
	}
})();
