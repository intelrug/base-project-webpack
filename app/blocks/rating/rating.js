(function rating() {
	var $trigger = $('.js-rating-trigger');

	$trigger.on('click', function () {
		var $scope = $(this).closest('.rating'),
			number = 5 - $(this).index();

		for (var i = 1; i < 6; i++) {
			$scope.removeClass('rating_marks_' + i);
		}

		$scope.addClass('rating_marks_' + number);
	});
})();
