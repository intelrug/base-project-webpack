(function ($) {
	$(function () {
		if (!String.prototype.startsWith) {
			String.prototype.startsWith = function (searchString, position) {
				position = position || 0;
				return this.indexOf(searchString, position) === position;
			};
		}

		console.log('common.js is ready');
		/* auto-injected code */
	});
})(jQuery);
