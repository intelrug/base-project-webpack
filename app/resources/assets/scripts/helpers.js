(function ($) {
	$(function () {
		'use strict';
		console.log('helpers.js is ready');



		(function toggle() {
			function parseAtribute(str) {
				if (typeof str === 'undefined') {
					return null;
				}

				if (str) {
					var arrayItems = str.split(',');
					var clearArrayItems = arrayItems.map(function (classString) {
						return classString.trim();
					});

					return clearArrayItems;
				}
			}

			var $trigger = $('.js-toggle');

			$trigger.on('click', function () {
				var toggleScope = $(this).data('toggleScope'),
					toggleTarget = $(this).data('toggleTarget'),
					toggleClass = $(this).data('toggleClass'),
					trigger = this;

				if (toggleScope && toggleTarget && toggleClass) {
					var scopeList = parseAtribute(toggleScope);
					var targetList = parseAtribute(toggleTarget);
					var classList = parseAtribute(toggleClass);

					if (scopeList.length && targetList.length && classList.length) {
						targetList.forEach(function (item, i) {
							if (typeof classList[i] !== 'undefined') {
								$(trigger).closest(scopeList[i]).find(targetList[i]).toggleClass(classList[i]);
							}
						});
					}
				}

				return false;
			});
		})();



		$('[name="tel"]').inputmask('+7(999) 999-99-99');



	});
})(jQuery);
