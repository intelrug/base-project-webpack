(function ($) {
	$(function () {
		'use strict';
		console.log('validate.js is ready');



		$('#demoForm').validate({
			errorClass: 'form__input_state_error',
			validClass: 'form__input_state_valid',
			rules: {
				firstName: {
					required: true
				},
				secondName: {
					required: true
				},
				tel: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				passwordRepeat: {
					required: true,
					equalTo: '#password'
				}
			},

			messages: {
				firstName: {
					required: ''
				},
				secondName: {
					required: ''
				},
				tel: {
					required: ''
				},
				email: {
					required: '',
					email: ''
				},
				password: {
					required: ''
				},
				passwordRepeat: {
					required: '',
					equalTo: ''
				}
			},

			errorPlacement: function(error, element) {

			},

			highlight: function(element, errorClass, validClass) {
				$(element).addClass(errorClass).removeClass(validClass);
			},

			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass(errorClass).addClass(validClass);
			}
		});



	});
})(jQuery);
