require('dotenv').config();
const colors = require('colors/safe');
const exec = require('child_process').exec;

const portfinder = require('portfinder');
const fs = require('fs');
const path = require('path');
const babel = require('@babel/core');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const rupture = require('rupture');
const autoprefixer = require('autoprefixer-stylus');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const WebpackConcatPlugin = require('./util/webpack/webpack-concat-files-plugin');
const concatCommonJsTransformer = require('./util/webpack/concat-common-js-transformer');
const HooksPlugin = require('hooks-webpack-plugin');

const DEV_SERVER_PORT = process.env.DEV_SERVER_PORT || 3000;

const pagesDir = './app/pages/';
const pages = fs.readdirSync(pagesDir).filter(fileName => fileName.endsWith('.pug'));

const LintOnDonePlugin = new HooksPlugin({
	'done@': (compiler, callback) => {
		console.log(colors.blue('Linting files...'));
		exec('cross-env FORCE_COLOR=1 NO_UPDATE_NOTIFIER=true npm run lint:p --silent -- -c', (err, stdout, stderr) => {
			if (stdout) {process.stdout.write(stdout);}
			if (stderr) {process.stderr.write(stderr);}
			if (stderr || stdout) {console.log(colors.red('Linting finished. Found errors.'));}
			else {console.log(
				colors.red('L')
				+ colors.red('i')
				+ colors.yellow('n')
				+ colors.yellow('t')
				+ colors.brightYellow('i')
				+ colors.brightYellow('n')
				+ colors.green('g ')
				+ colors.green('s')
				+ colors.blue('u')
				+ colors.blue('c')
				+ colors.brightBlue('c')
				+ colors.brightBlue('e')
				+ colors.magenta('s')
				+ colors.magenta('s')
			);}
		});
		callback();
	}
});

function getWebpackConfig(port) {
	return {
		entry: {
			app: path.resolve('./app/scripts/app.js'),
			grid: path.resolve('./app/scripts/grid.js')
		},
		output: {
			filename: './assets/scripts/[name].js'
		},
		performance: {
			maxEntrypointSize: 512000,
			maxAssetSize: 512000
		},
		devtool: false,
		devServer: {
			writeToDisk: true,
			quiet: true,
			contentBase: './dist',
			disableHostCheck: true,
			host: '0.0.0.0',
			public: `http://localhost:${port}`,
			port: parseInt(port, 10) + 1
		},
		resolve: {
			extensions: ['.js', '.pug', '.styl'],
			alias: {
				'@': path.resolve(__dirname, 'app')
			}
		},
		module: {
			rules: [
				{
					test: /\.styl$/,
					use: [
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
								hmr: process.env.NODE_ENV === 'development'
							}
						},
						'css-loader',
						{
							loader: 'stylus-loader',
							options: {
								preferPathResolver: 'webpack',
								use: [
									rupture(),
									autoprefixer()
								],
								import: [
									path.resolve(__dirname, 'app/styles/helpers/variables.styl'),
									path.resolve(__dirname, 'app/styles/helpers/mixins.styl')
								]
							}
						}
					]
				}, {
					test: /\.js$/,
					use: [
						'babel-loader',
						'webpack-import-glob-loader'
					]
				}, {
					test: /\.pug$/,
					use: [
						'raw-loader',
						'pug-html-loader?pretty=true'
					]
				}, {
					test: /\.svg$/,
					use: [
						{
							loader: 'svg-sprite-loader',
							options: {
								extract: true,
								spriteFilename: 'icon.svg',
								publicPath: './assets/images/'
							}
						},
						'svgo-loader'
					]
				}]
		},
		plugins: [
			new FriendlyErrorsPlugin(),
			new CleanWebpackPlugin(),
			new WebpackConcatPlugin({
				watchPaths: ['app/resources/assets/scripts/**/*-template.js'],
				bundles: [{
					source: [
						'app/blocks/**/*.js',
						'!app/blocks/**/global-*.js'
					],
					destination: './dist/assets/scripts/common.js',
					transforms: {
						after: async code => {
							try {
								return concatCommonJsTransformer(code);
							} catch (e) {
								console.error(e);
								return code;
							}
						}
					}
				}]
			}),
			new SpriteLoaderPlugin({
				plainSprite: true
			}),
			new CopyPlugin({
				patterns: [
					{
						from: 'app/resources', to: '',
						globOptions: {
							ignore: ['**/app/resources/assets/scripts/**/*.js']
						}
					},
					{
						from: 'app/resources/assets/scripts/*.js', to: 'assets/scripts/[name].[ext]',
						globOptions: {
							ignore: ['**/app/resources/assets/scripts/*-template.js']
						},
						transform(content) {
							const result = babel.transformSync(content, {filename: 'file.js'});
							return (result && result.code) || content;
						}
					},
					{
						from: 'app/blocks/**/*.+(png|jpg|jpeg|tiff|gif|bmp|ico)',
						to: 'assets/blocks/[1]/images/[2]/[name].[ext]',
						transformPath(targetPath, absolutePath) {
							const pattern = /blocks[\\\/](.*)[\\\/]images(([\\\/].*)?[\\\/]).*\..*$/;
							const matches = absolutePath.match(pattern);
							return targetPath.replace(/\[(\d+)]/g, (m, p1) => matches[parseInt(p1, 10)]);
						}
					}
				]}),
			new MiniCssExtractPlugin({
				filename: './assets/styles/[name].css',
				chunkFilename: '[id].css'
			}),
			...pages.map(page => new HtmlWebpackPlugin({
				filename: page.replace(/\.pug/, '.html'),
				template: pagesDir + page,
				minify: false
			})),
			new HtmlWebpackPugPlugin(),
			new BrowserSyncPlugin(
				{
					host: 'localhost',
					port,
					proxy: `http://localhost:${parseInt(port, 10) + 1}/`,
					open: false
				},
				{
					reload: false
				}
			),
			LintOnDonePlugin
		]
	};
}

module.exports = portfinder.getPortPromise({port: DEV_SERVER_PORT})
	.then(port => {
		return getWebpackConfig(port);
	});
